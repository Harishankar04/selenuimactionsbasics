using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumPractice
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {

        }
        [Test]
        public void TestbyID()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("C:\\Users\\Harishankar.S\\source\\repos\\Locator and Wait.html");
            driver.FindElement(By.Id("username")).SendKeys("HArishankar");
        }
        [Test]
        public void TestByName()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("C:\\Users\\Harishankar.S\\source\\repos\\Locator and Wait.html");
            IWebElement element = driver.FindElement(By.Name("username"));
            element.SendKeys("hari");
        }
        [Test]
        public void TestByTagName()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("C:\\Users\\Harishankar.S\\source\\repos\\Locator and Wait.html");
            driver.FindElement(By.TagName("input")).SendKeys("harishankarDass");
        }
        [Test]
        public void TestByClassName()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("C:\\Users\\Harishankar.S\\source\\repos\\Locator and Wait.html");
            driver.FindElement(By.ClassName("btnClass")).Click();
        }
        [Test]
        public void TestByLinkText()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("C:\\Users\\Harishankar.S\\source\\repos\\Locator and Wait.html");
            IWebElement Ltext = driver.FindElement(By.LinkText("Click me for Selenium"));
            Ltext.Click();
        }
        [Test]
        public void TestByXPath()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("C:\\Users\\Harishankar.S\\source\\repos\\Locator and Wait.html");
            IWebElement Attr = driver.FindElement(By.XPath("//label"));
            Console.WriteLine(Attr.Text);
            IWebElement Xpth = driver.FindElement(By.XPath("//input[@id='username']"));
            Xpth.SendKeys("HArishankar DAss S");
            IWebElement FElement = driver.FindElement(By.XPath("//label[text()='Username:']"));
            Console.WriteLine(FElement.Text);
            IWebElement FElement1 = driver.FindElement(By.XPath("//*[contains(@placeholder,'Enter')]"));
            FElement1.SendKeys("HAriahsgahgsagh");
            //IWebElement FElement2 = driver.FindElement(By.XPath(""));
        }
        [Test]
        public void TestByCssSelector()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("C:\\Users\\Harishankar.S\\source\\repos\\Locator and Wait.html");
            IWebElement Css1 = driver.FindElement(By.CssSelector("#username"));
            Css1.SendKeys("Harishankar");
            IWebElement Css2 = driver.FindElement(By.CssSelector(".btnClass"));
            Css2.Click();
            IWebElement Css3 = driver.FindElement(By.CssSelector("[placeholder=\"Enter your username\"]"));
            Css3.SendKeys("HAri");
            IWebElement Css4 = driver.FindElement(By.CssSelector("div#cssSelectorDemo .btnClass"));
            Css4.Click();
            IWebElement Css5 = driver.FindElement(By.CssSelector("[id^='btn']"));
            Css5.Click();
            IWebElement Css6 = driver.FindElement(By.CssSelector("div #btnId"));
            Css6.Click();
            IWebElement Css7 = driver.FindElement(By.CssSelector("div>#btnId"));
            Css7.Click();
            IWebElement Css8 = driver.FindElement(By.CssSelector("div :first-Child"));
            Console.WriteLine(Css8.Text);
            IWebElement Css9 = driver.FindElement(By.CssSelector("div :last-Child"));
            Console.WriteLine(Css9.Text);
        }
        [Test]
        public void ImplicitWaits()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("C:\\Users\\Harishankar.S\\SeleniumTraining\\Locator and Wait 1.html");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            IWebElement element = driver.FindElement(By.XPath("//button[contains(text(),'Click For')]"));
            element.Click();
        }
        [Test]
        public void ExplicitWait()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("C:\\Users\\Harishankar.S\\SeleniumTraining\\Locator and Wait 1.html");
            IWebElement element = driver.FindElement(By.XPath("//button[contains(text(),'Click For')]"));
            element.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(driver => driver.FindElement(By.XPath("//*[contains(text(),'Button')][1]")).Text == "Button Clicked!");
        }
        [Test]
        public void FluentWait()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("C:\\Users\\Harishankar.S\\SeleniumTraining\\Locator and Wait 1.html");
            IWebElement element = driver.FindElement(By.XPath("//button[contains(text(),'Click For')]"));
            element.Click();
            DefaultWait<IWebDriver> fluentWait = new DefaultWait<IWebDriver>(driver)
            {
                Timeout = TimeSpan.FromSeconds(10),
                PollingInterval = TimeSpan.FromMilliseconds(5)
            };
            fluentWait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            fluentWait.Until(x => driver.FindElement(By.XPath("//*[contains(text(),'Button')][1]")).Text == "Button Clicked!");
        }
    }
}