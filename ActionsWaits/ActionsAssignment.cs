using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace ActionsWaits
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ActionAssignment()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("file:///C:/Users/Harishankar.S/SeleniumTraining/home.html");
            driver.FindElement(By.XPath("//a[contains(text(),'Login')]")).Click();
            driver.SwitchTo().Frame(driver.FindElement(By.Id("loginFrame")));
            driver.FindElement(By.Id("username")).SendKeys("harishankar@gmail.com");
            driver.FindElement(By.XPath("//*[contains(@id,'password')]")).SendKeys("akakaka");
            driver.FindElement(By.Id("loginBtn")).Click();
            IAlert alert = driver.SwitchTo().Alert();
            Console.WriteLine(alert.Text);
            alert.Accept();
        }
    }
}