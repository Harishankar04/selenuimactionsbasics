using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace ActionsPracticeExercise
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void DragAndDropAction()
        {
            WebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("file:///C:/Users/Harishankar.S/SeleniumTraining/home.html");
            driver.FindElement(By.XPath("//a[contains(text(),'Drag')]")).Click();
            IWebElement draggable = driver.FindElement(By.CssSelector("div #dragText"));
            IWebElement drop = driver.FindElement(By.Id("box1"));
            Actions act = new Actions(driver);
            draggable.Clear();
            draggable.SendKeys("Harishankar");
            act.ClickAndHold(draggable);
            act.MoveToElement(drop);
            act.Release();
            act.Build();
            act.Perform();
            IWebElement dragbox = driver.FindElement(By.CssSelector(".draggable"));
            IWebElement dropbox = driver.FindElement(By.Id("droppableElement"));
            act.DragAndDrop(dragbox, dropbox).Perform();
        }
        [Test]
        public void ButtonAndProgressBar()
        {
            WebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("file:///C:/Users/Harishankar.S/SeleniumTraining/home.html");
            driver.FindElement(By.XPath("//a[contains(text(),'Button')]")).Click();
            Actions act = new Actions(driver);
            IWebElement DClick = driver.FindElement(By.Id("doubleClickBtn"));
            IWebElement CHoldme = driver.FindElement(By.XPath("//*[contains(text(),'Click and Hold Me')]"));
            IWebElement MvElement = driver.FindElement(By.CssSelector("#moveToElementBtn"));
            IWebElement ContextClck = driver.FindElement(By.Id("contextClickBtn"));
            act.DoubleClick(DClick).Perform();
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();
            act.ClickAndHold(CHoldme).Perform();
            Thread.Sleep(4000);
            alert.Accept();
            act.MoveToElement(MvElement).Perform();
            Thread.Sleep(1000);
            alert.Accept();
            act.ContextClick(ContextClck).Perform();
            for(int i=0;i<7;i++)
            {
                act.SendKeys(Keys.ArrowUp).Perform();
            }
            System.Threading.Thread.Sleep(1000);
            for (int i = 0; i < 3; i++)
            {
                act.SendKeys(Keys.ArrowDown).Perform();
            }
        }
        [Test]
        public void FormElements()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("file:///C:/Users/Harishankar.S/SeleniumTraining/home.html");
            driver.FindElement(By.XPath("//a[contains(text(),'Form')]")).Click();
            IWebElement MultiSelect = driver.FindElement(By.Id("dropdown"));
            SelectElement selectoptions = new SelectElement(MultiSelect);
            selectoptions.SelectByIndex(0);
            selectoptions.SelectByIndex(2);
            IWebElement Dropdown1 = driver.FindElement(By.XPath("//div[2]/select"));
            SelectElement SingleSelect = new SelectElement(Dropdown1);
            SingleSelect.SelectByIndex(1);
            IWebElement radio1 = driver.FindElement(By.Id("radioButton1"));
            IWebElement radio2 = driver.FindElement(By.Id("radioButton2"));
            radio1.Click();
            if (!radio1.Selected)
            {
                radio2.Click();
            }
            IWebElement CheckBox = driver.FindElement(By.Id("checkbox"));
            if (!CheckBox.Selected)
                CheckBox.Click();
        }
        [Test]
        public void AlertExamples()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("file:///C:/Users/Harishankar.S/SeleniumTraining/home.html");
            driver.FindElement(By.XPath("//a[contains(text(),'Alert')]")).Click();
            driver.FindElement(By.XPath("//button[contains(text(),'Simple Alert')]")).Click();
            IAlert alert = driver.SwitchTo().Alert();
            Thread.Sleep(1000);
            alert.Accept();
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("//button[contains(text(),'Confirm Alert')]")).Click();
            Thread.Sleep(1000);
            alert.Dismiss();
            Thread.Sleep(1000);
            alert.Accept();
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("//button[contains(text(),'Prompt Alert')]")).Click();
            Thread.Sleep(1000);
            alert.SendKeys("Harishankar");
            Thread.Sleep(1000);
            alert.Accept();
            Thread.Sleep(1000);
            alert.Accept();
        }
        [Test]
        public void WindowHandle()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("file:///C:/Users/Harishankar.S/SeleniumTraining/home.html");
            driver.FindElement(By.XPath("//a[contains(text(),'Window')]")).Click();
            driver.FindElement(By.Id("openNewWindowBtn")).Click();
            Thread.Sleep(1000);
            var WindowHandle = driver.WindowHandles;
            driver.SwitchTo().Window(WindowHandle[1]);
            driver.Close();
            Thread.Sleep(1000);
            driver.SwitchTo().Window(WindowHandle[0]);
            driver.FindElement(By.Id("openNewTabBtn")).Click();
            Thread.Sleep(1000);
            var WindowHandle2 = driver.WindowHandles;
            driver.SwitchTo().Window(WindowHandle2[1]);
            driver.Close();
            Thread.Sleep(1000);
            driver.SwitchTo().Window(WindowHandle2[0]);
        }
    }
}