using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Assignment1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void AmazonTestById()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.amazon.in/");
            IWebElement Element = driver.FindElement(By.Id("twotabsearchtextbox"));
            Element.SendKeys("MobilePhones");
        }
        [Test]
        public void AmazonTestByName()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.amazon.in/");
            IWebElement Element = driver.FindElement(By.Name("field-keywords"));
            Element.SendKeys("Accessories");
        }
        [Test]
        public void AmazonTestByClass()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("file:///C:/Users/Harishankar.S/source/repos/Locator%20and%20Wait.html");
            IWebElement Element = driver.FindElement(By.ClassName("btnClass"));
            Element.Click();
        }
        [Test]
        public void AmazonTestByXpath()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.amazon.in/");
            IWebElement Element = driver.FindElement(By.XPath("//input[@placeholder='Search Amazon.in']"));
            Element.SendKeys("Mobile Phones");
        }
        [Test]
        public void AmazonTestByCssSelector()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.amazon.in/");
            IWebElement element = driver.FindElement(By.CssSelector("[placeholder^='Sear']"));
            element.SendKeys("LaptopBags");
        }
        
    }
}