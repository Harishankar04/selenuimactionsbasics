using POM.Driver;
using POM.WebPageClasses;

namespace POM
{
    public class POMSeleniumTest : Driver.DriverClass
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void POMLoginTest()
        {
            LoginPage login = new LoginPage(Driver);
            login.HomeURLLoading();
            Thread.Sleep(2000);
            login.LoginModule();
            Thread.Sleep(2000);
            login.CloseDriver();
        }
        [Test]
        public void POMDragAndDropTest()
        {
            DragAndDrop DragDrop = new DragAndDrop(Driver);
            DragDrop.HomeURLLoading();
            Thread.Sleep(1000);
            DragDrop.DragAndDropModule();
            Thread.Sleep(1000);
            DragDrop.CloseDriver();
        }
        [Test]
        public void ButtonAndProgressBarTest()
        {
            ButtonProgressbar BtnProgress = new ButtonProgressbar(Driver);
            BtnProgress.HomeURLLoading();
            Thread.Sleep(1000);
            BtnProgress.ButtonAndProgressModule();
            Thread.Sleep(1000);
            BtnProgress.CloseDriver();
        }
        [Test]
        public void FormElementTest()
        {
            FormElements FE = new FormElements(Driver);
            FE.HomeURLLoading();
            Thread.Sleep(1000);
            FE.FormElementModule();
            Thread.Sleep(1000);
            FE.CloseDriver();
        }
        [Test]
        public void AlertExampleTest()
        {
            AlertExample AE = new AlertExample(Driver);
            AE.HomeURLLoading();
            Thread.Sleep(1000);
            AE.AlertExampleModule();
            Thread.Sleep(1000);
            AE.CloseDriver();
        }
        [Test]
        public void WindowHandleTest()
        {
            WindowHandle WH = new WindowHandle(Driver);
            WH.HomeURLLoading();
            Thread.Sleep(1000);
            WH.WindowHandleModule();
            Thread.Sleep(1000);
            WH.CloseDriver();
        }
    }
}