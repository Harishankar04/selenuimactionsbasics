﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POM.Driver
{
    public class DriverClass
    {
        protected IWebDriver Driver;
        public DriverClass()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
        }
    }
}
