﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.WaitHelpers;
using POM.WebPageClasses;

namespace POM.BaseClass
{
    public class BaseClass
    {
        protected IWebDriver BaseDriver;
        public BaseClass(IWebDriver driver)
        {
            BaseDriver = driver;
        }
        public void HomeURLLoading()
        {
            BaseDriver.Navigate().GoToUrl("file:///C:/Users/Harishankar.S/SeleniumTraining/home.html");
        }
        public void CloseDriver()
        {
            BaseDriver.Quit();
        }
        public void Click(IWebElement ele)
        {
            WebDriverWait wait = new WebDriverWait(BaseDriver, TimeSpan.FromSeconds(5));
            wait.Until(ExpectedConditions.ElementToBeClickable(ele));
            ele.Click();
        }
        public void SelectFromDropDown(IWebElement dropdown)
        {

        }
        public void AlertAccept()
        {
            IAlert alert = BaseDriver.SwitchTo().Alert();
            alert.Accept();
        }
        public void AlertDismiss()
        {
            IAlert alert = BaseDriver.SwitchTo().Alert();
            alert.Dismiss();
        }
        public void AlertHandle()
        {
            WebDriverWait wait = new WebDriverWait(BaseDriver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.AlertIsPresent());
        }
        public void PAlert(string input)
        {
            IAlert alert = BaseDriver.SwitchTo().Alert();
            alert.SendKeys(input);
            alert.Accept();
        }
        public void NewWindowHandle()
        {
            var WindowHandle = BaseDriver.WindowHandles;
            BaseDriver.SwitchTo().Window(WindowHandle[1]);
        }
        public void MainWindow()
        {
            var WindowHandle = BaseDriver.WindowHandles;
            BaseDriver.SwitchTo().Window(WindowHandle[0]);
        }
    }
}
