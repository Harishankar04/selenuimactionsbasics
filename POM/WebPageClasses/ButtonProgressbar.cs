﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POM.WebPageClasses
{
    public class ButtonProgressbar : BaseClass.BaseClass
    {
        public ButtonProgressbar(IWebDriver driver) : base(driver)
        {
        }
        public void ButtonAndProgressModule()
        {
            IWebElement ButtonProgressLink = BaseDriver.FindElement(By.XPath("//a[contains(text(),'Button')]"));
            IWebElement DblclickButton = BaseDriver.FindElement(By.XPath("//div/button[@ondblclick='doubleClickEvent()']"));
            IWebElement ClickAndHoldBtn = BaseDriver.FindElement(By.Id("clickAndHoldBtn"));
            IWebElement MovetoElementBtn = BaseDriver.FindElement(By.Id("moveToElementBtn"));
            IWebElement ContextClickBtn = BaseDriver.FindElement(By.Id("contextClickBtn"));
            ButtonProgressLink.Click();
            Actions acts = new Actions(BaseDriver);
            acts.DoubleClick(DblclickButton).Perform();
            AlertAccept();
            acts.ClickAndHold(ClickAndHoldBtn).Perform();
            AlertHandle();
            AlertAccept();
            acts.MoveToElement(MovetoElementBtn).Perform();
            AlertAccept();
            acts.ContextClick(ContextClickBtn).Perform();
            for(int i= 0;i<7;i++)
            {
                acts.SendKeys(Keys.ArrowUp).Perform();
            }
            Thread.Sleep(2000);
            for(int i= 0;i<3;i++)
            {
                acts.SendKeys(Keys.ArrowDown).Perform();
            }
        }
    }
}
