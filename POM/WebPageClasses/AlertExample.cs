﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POM.WebPageClasses
{
    public class AlertExample :BaseClass.BaseClass
    {
        public AlertExample(IWebDriver driver) : base(driver)
        {
        }
        public void AlertExampleModule()
        {
            BaseDriver.FindElement(By.XPath("//a[contains(text(),'Alert')]")).Click();
            IWebElement SimpleAlert = BaseDriver.FindElement(By.XPath("//div/button[@onclick='showSimpleAlert()']"));
            IWebElement ConfirmAlert = BaseDriver.FindElement(By.XPath("//div/button[@onclick='showConfirmAlert()']"));
            IWebElement PromptAlert = BaseDriver.FindElement(By.XPath("//div/button[@onclick='showPromptAlert()']"));
            Click(SimpleAlert);
            AlertAccept();
            Click(ConfirmAlert);
            AlertDismiss();
            AlertAccept();
            Click(PromptAlert);
            Thread.Sleep(1000);
            PAlert("Hari");
        }
    }
}
