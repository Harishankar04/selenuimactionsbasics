﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POM.WebPageClasses
{
    public class WindowHandle : BaseClass.BaseClass
    {
        public WindowHandle(IWebDriver driver):base(driver)
        {
        }
        public void WindowHandleModule()
        {
            BaseDriver.FindElement(By.XPath("//a[contains(text(),'Window')]")).Click();
            IWebElement NewWindow = BaseDriver.FindElement(By.Id("openNewWindowBtn"));
            IWebElement NewTab = BaseDriver.FindElement(By.Id("openNewTabBtn"));
            Click(NewWindow);
            NewWindowHandle();
            BaseDriver.Close();
            MainWindow();
            Click(NewTab);
            NewWindowHandle();
            MainWindow();
        }
    }
}
