﻿using OpenQA.Selenium;
using OpenQA.Selenium.DevTools.V119.Performance;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POM.WebPageClasses
{
    public class DragAndDrop : BaseClass.BaseClass
    {
        public DragAndDrop(IWebDriver driver) : base(driver)
        {
        }
        public void DragAndDropModule()
        {
            IWebElement DragAndDropLink = BaseDriver.FindElement(By.XPath("//a[contains(text(),'Drag')]"));
            IWebElement Draggable = BaseDriver.FindElement(By.XPath("//*[@id='dragText']"));
            IWebElement DropArea = BaseDriver.FindElement(By.Id("box1"));
            IWebElement DragBox = BaseDriver.FindElement(By.CssSelector(".draggable"));
            IWebElement DropBox = BaseDriver.FindElement(By.CssSelector(".droppable"));
            DragAndDropLink.Click();
            Draggable.Clear();
            Draggable.SendKeys("Type something and it will be moved to the drop box");
            Actions acts = new Actions(BaseDriver);
            acts.ClickAndHold(Draggable);
            acts.MoveToElement(DropArea);
            acts.Release();
            acts.Build();
            acts.Perform();
            acts.DragAndDrop(DragBox, DropBox);
            acts.Perform();
        }
    }
}
