﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POM.WebPageClasses
{
    public class LoginPage : BaseClass.BaseClass
    {
        public LoginPage(IWebDriver driver) : base(driver)
        {
        }
        public void LoginModule()
        {
            IWebElement Loginlink = BaseDriver.FindElement(By.XPath("//a[contains(text(),'Login')]"));
            Loginlink.Click();
            BaseDriver.SwitchTo().Frame(BaseDriver.FindElement(By.Id("loginFrame")));
            IWebElement username = BaseDriver.FindElement(By.Id("username"));
            IWebElement password = BaseDriver.FindElement(By.XPath("//*[contains(@id,'password')]"));
            IWebElement loginBtn = BaseDriver.FindElement(By.Id("loginBtn"));
            username.SendKeys("HariTest");
            password.SendKeys("Harishankar");
            Click(loginBtn);
            AlertAccept();
        }
    }
}
