﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Numerics;

namespace POM.WebPageClasses
{
    public class FormElements : BaseClass.BaseClass
    {
        public FormElements(IWebDriver driver):base(driver)
        {
        }
        public void FormElementModule()
        {
            BaseDriver.FindElement(By.XPath("//a[contains(text(),'Form')]")).Click();
            IWebElement Multiselect = BaseDriver.FindElement(By.Id("dropdown"));
            SelectElement multipleoption = new SelectElement(Multiselect);
            multipleoption.SelectByIndex(0);
            multipleoption.SelectByIndex(2);
            IWebElement singleSelect = BaseDriver.FindElement(By.CssSelector(".dropdown"));
            SelectElement SS = new SelectElement(singleSelect);
            SS.SelectByIndex(1);
            IWebElement Radio1 = BaseDriver.FindElement(By.Id("radioButton1"));
            IWebElement Radio2 = BaseDriver.FindElement(By.Id("radioButton2"));
            Radio1.Click();
            if (!Radio1.Selected)
            {
                Radio2.Click();
            }
            IWebElement CheckBox = BaseDriver.FindElement(By.Id("checkbox"));
            CheckBox.Click();
        }
    }
}
